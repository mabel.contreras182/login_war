/**
 * Created by xavi on 5/16/17.
 */
import {Component, OnInit} from "@angular/core";
import {StorageService} from "../core/services/storage.service";
import {User} from "../core/models/user.model";
import {AuthenticationService} from "../login/shared/authentication.service";
@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
  public user: User;
  public welcomeText: String;

  constructor(
    private storageService: StorageService,
    private authenticationService: AuthenticationService
  ) { }
    
   saludar(){
    return "Bienvenido "+this.user.name + " " +this.user.surname;
    }
       
  ngOnInit() {
    this.user = this.storageService.getCurrentUser();
    this.welcomeText = this.saludar();
  }

  public logout(): void{
    this.authenticationService.logout().subscribe(
        response => {if(response) {this.storageService.logout();}}
    );
  }

}
